package T.HAUTIER;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.JLayeredPane;

public class Client {

	public JFrame frame;
	public JPanel panel_Login;
	public JLabel JlabTitle;
	public JButton btnOk;
	public JPasswordField FieldPassword;
	public JTextField FieldUsername;
	public JLabel JlabUsername;
	public JLabel JlabPassword;
	public JPanel panel_Logged;
	public JTextArea txtrNom;
	public JLabel JlabTitle2;
	public JButton btnNewButton;
	
	public FrameFeedback fb;
	
	/**
	 * Create the application.
	 */
	public Client() {
		initialize();
	}
	/**
	* Constructeur de la fenetre sans parametre(s)
	*/
	private void client() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 250, 275);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
	}
	/**
	 * Initialisation des parametre(s) et contenu du client
	 */
	private void initialize() {
		client();
		/**
		 * Initiaisation du JLayeredPanl
		 */
		JLayeredPane layeredPane = new JLayeredPane();
		layeredPane.setBounds(0, 0, 244, 246);
		frame.getContentPane().add(layeredPane);
		/**
		 * Initiaisation du JPanel de login
		 */
		panel_Login = new JPanel();
		panel_Login.setLayout(null);
		panel_Login.setBackground(Color.PINK);
		panel_Login.setBounds(0, 0, 244, 246);
			layeredPane.add(panel_Login);
			/**
			 * Initiaisation du JButton Ok
			 */
		btnOk = new JButton("Ok !");
		btnOk.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				fb.login(FieldUsername.getText(), FieldPassword.getText());
				
				layeredPane.removeAll();
				layeredPane.add(panel_Logged);
				layeredPane.repaint();
				layeredPane.revalidate();
				
			}
		});
		/**
		 * Initiaisation du JLabel jlabTitle
		 */
		JlabTitle = new JLabel("Agence Ard\u20ACnnes");
		JlabTitle.setHorizontalAlignment(SwingConstants.CENTER);
		JlabTitle.setBounds(22, 0, 200, 14);
		panel_Login.add(JlabTitle);
		
		JlabUsername = new JLabel("Nom du compte");
		JlabUsername.setBounds(72, 51, 100, 14);
		panel_Login.add(JlabUsername);
		
		FieldUsername = new JTextField();
		FieldUsername.setColumns(10);
		FieldUsername.setBounds(72, 65, 100, 20);
		panel_Login.add(FieldUsername);
		
		JlabPassword = new JLabel("Mot de passe");
		JlabPassword.setBounds(71, 96, 101, 14);
		panel_Login.add(JlabPassword);
		
		FieldPassword = new JPasswordField();
		FieldPassword.setBounds(72, 111, 100, 20);
		panel_Login.add(FieldPassword);
		btnOk.setBounds(77, 173, 89, 23);
		panel_Login.add(btnOk);
		
		panel_Logged = new JPanel();
		panel_Logged.setLayout(null);
		panel_Logged.setBackground(Color.GRAY);
		panel_Logged.setBounds(0, 0, 244, 246);
			//layeredPane.add(panel_Logged);
		
		txtrNom = new JTextArea();
		txtrNom.setBackground(Color.WHITE);
		txtrNom.setText("Nom     : ??\r\nPrenom  : ??\r\n--------------------------------------------\r\nCompte n\u00B0 ??\r\nSolde : ???\r\nCompte n\u00B0 ??\r\nSolde : ??");
		txtrNom.setLineWrap(true);
		txtrNom.setEditable(false);
		txtrNom.setBounds(21, 35, 201, 156);
		panel_Logged.add(txtrNom);
		
		JlabTitle2 = new JLabel("Agence Ard\u20ACnnes");
		JlabTitle2.setHorizontalAlignment(SwingConstants.CENTER);
		JlabTitle2.setBounds(22, 0, 200, 14);
		panel_Logged.add(JlabTitle2);
		
		btnNewButton = new JButton("Deconnect");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				fb.logout();
				
				layeredPane.removeAll();
				layeredPane.add(panel_Login);
				layeredPane.repaint();
				layeredPane.revalidate();
				
			}
		});
		btnNewButton.setBounds(77, 202, 89, 23);
		panel_Logged.add(btnNewButton);
	}

	public void addFeedback(FrameFeedback fb) {
		this.fb = fb;
	}
}
