package T.HAUTIER;

import java.awt.EventQueue;


public class Main {
/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Client window = new Client();
					SIO2_TEST_JDBC jdbc = new SIO2_TEST_JDBC(window);
					window.addFeedback(jdbc);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}