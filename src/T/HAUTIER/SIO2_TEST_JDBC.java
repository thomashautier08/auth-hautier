package T.HAUTIER;

import java.sql.*;
public class SIO2_TEST_JDBC implements FrameFeedback
{
	Client ObjClient;
	
	Connection connect;
	Statement connectionMySQL;
	
	String URL = "jdbc:mysql://127.0.0.1:3306/BD_SUCRE",
	       UTILISATEUR = "root",
	       MOTdePASSE = "root",
		   REQUETE = "SELECT * FROM clients",
		   DRIVER = "com.mysql.cj.jdbc.Driver";
	

	public SIO2_TEST_JDBC(Client window) {
		this.ObjClient = window;
		
		try {
			DatabaseInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void DatabaseInit() throws Exception
	{
		// Chargement de la librairie Java/MySQL
		Class.forName(DRIVER).newInstance();
		connect = DriverManager.getConnection(URL, UTILISATEUR, MOTdePASSE);
		connectionMySQL = (Statement)connect.createStatement();
	}
	
	public void TEST() {
		try {
			//Requête SQL
			ResultSet reqResult = connectionMySQL.executeQuery(REQUETE);

			while(reqResult.next()) { //Equivalent à fetch en PHP
				String lel = reqResult.getString("VALEUR");
				System.out.println(lel);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	@Override
	public boolean login(String user, String password) {
		System.out.println("DEBUG : LOGIN with User("+user+") and Password("+password+")");
		
		//TEST();
		
		return true; //Confirme ou non l'acces
	}

	@Override
	public void logout() {
		System.out.println("DEBUG : LOGOUT");
		
		// Fermeture de la connexion
		try {
			connectionMySQL.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}